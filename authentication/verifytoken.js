const router = require('express').Router();
const privatekey = require('./privatekey').key;
const jwt = require('jsonwebtoken');


router.post('/verifytoken',(req,res)=>
{
    const token = req.body.token;
    try
    {
        let verifiedtoken = jwt.verify(token,privatekey)
        jwt.sign({username:verifiedtoken.username},privatekey,{expiresIn:'5m'},(error,token_)=>
        {s
            res.status(200).json({token:token_});
        });
    }
    catch(error)
    {
        res.status(200).json({token:""});
    }   
    
})

module.exports = router;