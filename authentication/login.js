const router = require('express').Router();
const privatekey = require('./privatekey').key;
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const model = require('../database/model');


router.post('/login',(req,res)=>
 {
    let user_name = req.body.user;
    let pass_word = req.body.pass;
    let userid;
    
    let decryptpassword = async (password,hashedpassword)=>
    {   
        let match;
        try
        {
            match = await bcrypt.compare(password,hashedpassword);
        }
        catch(error)
        {
            console.log(error)
        }
        if(match)
            furtherauth();  
        else 
            return res.status(404).json({token:"",message:"Wrong credentials"});
    }
  
    let findhashedpassword = async()=>
    {
        let userinfo;
        try
        { 
            userinfo =  await model.findOne({username:user_name});
            
        }
        catch(error)
        {
            console.log(error);
        }
       
        
        if(userinfo)
        {
            userid = userinfo.id;
            decryptpassword(pass_word,userinfo.password);
        }
        else
            return res.status(404).json({token:"",message:"Wrong credentials"});   
    }
    findhashedpassword();
 
    
    let furtherauth = ()=>
    {
        jwt.sign({username:userid},privatekey,{expiresIn:'10m'},(error,token_)=>
        {
            return res.status(200).json({username:user_name,token:token_});
        });
    }
});

module.exports = router;