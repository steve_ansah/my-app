const mongoose = require('mongoose');


mongoose.connect("mongodb://mongo/todos",{ useUnifiedTopology:true,useNewUrlParser:true},);
mongoose.set('useUnifiedTopology', true);   
mongoose.connection.once('open',()=>{
    console.log("database connection succeeded")
}).on('error',(error)=>{
    console.log("Couldn't connect to database",error);
});
module.exports = mongoose;