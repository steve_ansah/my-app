const mongoose = require('mongoose');

const userschema = new mongoose.Schema({
    username:
    {
       type:String,
       required:true,
       trim:true,
   
    },
    password:
    {
        type:String,
        required:true,
        trim:true,
    },
    firstname:String,
    surname:String,
});
const usermodel = mongoose.model("users",userschema);

module.exports = usermodel