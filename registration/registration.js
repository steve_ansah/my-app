const router = require('express').Router();
const  model = require('../database/model');
const bcrypt = require('bcrypt')

router.post('/register',(req,res)=>
{
    let user_name = req.body.username;
    let pass_word = req.body.password;
    let firstname = req.body.firstname;
    let surname = req.body.surname;
  
    let checkexists = async() =>
    {
        let response;
        try
        {
            response = await model.findOne({username:user_name});
        }
        catch(error)
        {
            console.log(error);
        }
        if(response)
        {
            return res.status(404).json({success:false,message:"Username already exist"});
        }
        saveuser();
    }
    checkexists();
    let saveuser = async()=>
    {
        const saltrounds = 10;
        let hashedpassword 
        try
        {
           hashedpassword = await bcrypt.hash(pass_word,saltrounds);
        }
        catch(error)
        {
            console.log(error);
        }
        let newuser = new model({
            username:user_name,
            password:hashedpassword,
            firstname:firstname,
            surname:surname
        });
        let successful;
        try
        {

            successful  = await newuser.save();
        }
        catch(error)
        {
            console.log(error);
        }
        if(successful)
            res.status(200).json({success:true,message:"registration succesful"});
        else
            res.status(404).json({success:true,message:"registration not completed"});    
    }
});

module.exports = router;