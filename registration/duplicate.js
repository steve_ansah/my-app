const router = require('express').Router();
var model = require('../database/model');


router.post('/exists',(req,res)=>
{
    let user = req.body.username;
    try
    {
        let checkforexistinguser =  async() =>
        {
            let response = await model.findOne({username:user});
            if(response)
            {
                return res.status(200).json({success:false,message:"Username already exist"});
            }
            return res.status(200).json({success:true,message:user+" available"});
        }
        checkforexistinguser();
    }
    catch(error)
    {
        console.log(error);
    }
});

module.exports = router;