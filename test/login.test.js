const assert = require('chai').should();
const app = require('../app');
const request = require('supertest')

describe("POST /login",()=>
{
    it("should sign in the user and report status code 200",done =>
    {
        const user = {"user":"roger_123456","pass":"password"};
         request(app).post('/login')
        .send(user)
        .set('Accept','application/json')
        .expect('Content-Type',/json/)
        .expect(200,done);
    })
    it("should report 404 if user does not exist ",done =>
    {
        const user = {username:"roger",password:"passwordexample"}; 
        request(app).post("/login")
        .send(user)
        .set('Accept','application/json')
        .expect('Content-Type',/json/)
        .expect(res=>{res.body.token.should.equal("")})
        .expect(404,done)
    });
   
   
})