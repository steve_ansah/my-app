const app = require('../app');
const request = require('supertest')

describe("POST /register",()=>
{
    it("should register the user and report status code 200",done =>
    {
        const user = {"username":"roger_123456","password":"password","firstname":"finn","surname":"stanley"};
         request(app).post('/register')
        .send(user)
        .set('Accept','application/json')
        .expect('Content-Type',/json/)
        .expect(200,done);
    })
    it("should report 404 if user already exists ",done =>
    {
        const user = {"username":"roger_123456","password":"password","firstname":"finn","surname":"stanley"};
        request(app).post('/register')
        .send(user)
        .set('Accept','application/json')
        .expect('Content-Type',/json/)
        .expect(404,done);
    });
   
   
})