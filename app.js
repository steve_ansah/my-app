const express = require('express');
const bodyparser = require('body-parser');
const login = require('./authentication/login');
const verifytoken = require('./authentication/verifytoken')
const register = require('./registration/registration');
const duplicate = require('./registration/duplicate');
const cors = require('cors');
const app = express();
const database = require('./database/database');
const privatekey = require('./authentication/privatekey').key;

const corsoption = {
    origin:'https://localhost',
    credentials:true,
}
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended:true}));
app.use(cors('*'));
let PORT = 4000;
app.use(login);
app.use(register);
app.use(duplicate);
app.use(verifytoken);


app.listen(process.env.PORT || PORT,()=>
{
    console.log('server listening on port '+PORT);
});

module.exports = app;